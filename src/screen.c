#include "screen.h"

void clear_screen(){
	for(uint16_t i = VGA_START_ADDR; i <= VGA_END_ADDR; i++){
		uint16_t data = 0x00000;
		uint16_t *vgaptr = (uint16_t *)i;
		*vgaptr = data;
	}
	set_cursor(0,0);
}

void set_cursor(uint8_t row, uint8_t column){
	cursor_row = row;
	cursor_column = column;
}

void get_cursor(uint8_t *row, uint8_t *column){
	row = &cursor_row;
	column = &cursor_column;
}

void set_color(uint8_t fgcolor, uint8_t bgcolor){
	foreground = fgcolor;
	background = bgcolor;
}

void get_color(uint8_t *fgcolor, uint8_t *bgcolor){
	fgcolor = &foreground;
	bgcolor = &background;
}

void put_char(uint8_t ch){
	uint16_t config = 0x8b00;
	uint16_t data = config | ch;
	uint16_t *vgaptr = (uint16_t *)(VGA_START_ADDR + ((uint16_t) 80 * cursor_row + cursor_column));
	*vgaptr = data;
}

void puts(char *str){
	int size = 0;
	for(int i = 0; str[i] != '\0'; i++){
		size++;
	}
	for(int i = 0; i < size; i++){
		put_char(str[i]);
		if(cursor_column < (COLUMN*ROW)){
			cursor_column = cursor_column+1;
		}else if(cursor_column >= COLUMN){
			cursor_row = cursor_row + 1;
		}	
	}
}

void put_decimal(uint32_t n){
		char x = (char)n  + 48;
		put_char(x);

}