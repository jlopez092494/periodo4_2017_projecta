#ifndef SCREEN_H
#define SCREEN_H
	
#include "system.h"

#define ROW 30
#define COLUMN 80

uint8_t cursor_row;
uint8_t cursor_column;
uint8_t foreground;
uint8_t background;

void clear_screen();

void set_cursor(uint8_t row, uint8_t column);

void get_cursor(uint8_t *row, uint8_t *column);

void set_color(uint8_t fgcolor, uint8_t bgcolor);

void get_color(uint8_t *fgcolor, uint8_t *bgcolor);

void put_char(uint8_t ch);

void puts(char *str);

void put_decimal(uint32_t n);

//80y + x = f
#endif